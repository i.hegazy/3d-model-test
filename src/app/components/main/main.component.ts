import { Component, OnInit, ElementRef, ViewChild, OnDestroy } from '@angular/core';
import { EngineService } from 'src/app/engine.service';
import { Subscription } from 'rxjs';
import { NgForm } from '@angular/forms';
import * as THREE from 'three';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit, OnDestroy {

  @ViewChild('rendererCanvas', { static: true })
  public rendererCanvas: ElementRef<HTMLCanvasElement>;
  sub: Subscription;
  skinMale = {
    skinWhite: {
      images: [
        "assets/characters/man/body/basic/skinColorWhite/man_face_white.jpg",
        "assets/characters/man/body/basic/skinColorWhite/man_west_white.jpg",
        "assets/characters/man/body/basic/skinColorWhite/man_leg_white.jpg",
      ],
      children: [
        "base_0",
        "base_1",
        "base_2"
      ]

    },
    skinBlack: {
      images: [
        "assets/characters/man/body/basic/skinColorBlack/man_face_black.jpg",
        "assets/characters/man/body/basic/skinColorBlack/man_west_black.jpg",
        "assets/characters/man/body/basic/skinColorBlack/man_leg_black.jpg",
      ],
      children: [
        "base_0",
        "base_1",
        "base_2"
      ]
    }
  };

  trousermale = {
    style1: {
      image:
        ['assets/characters/man/clothes/trouser/Polyester_Viscose_Mens_Trouser.jpg'],
      children: ["trouser_0", "trouser_1", "trouser_2", "trouser_3"]

    }
  }

  tshirtMale = {
    style1: {
      image:
        ['assets/characters/man/clothes/tshirt/Jersey_garay.jpg', "assets/characters/man/clothes/tshirt/Marvelous Desinger_emblem.png"],
      children: ["tshirt_0", "tshirt_1"]

    }

  }
  constructor(private engServ: EngineService) { }
  ngOnInit() {
    this.engServ.getCanvas(this.rendererCanvas);
    this.engServ.createRenderer();
    this.engServ.createScene();
    this.engServ.createCamera();
    this.engServ.createLight();
    this.engServ.animate();
    this.engServ.addControls();

    //this.engServ.loadModel('assets/characters/man/body/hair/mHairBlack.glb');
    this.engServ.loadModel('assets/characters/man/body/basic/mBody.glb');
    // this.engServ.loadModel('assets/characters/man/body/eays/mEays.gltf');
    // this.engServ.loadModel('assets/characters/man/clothes/tshirt/tshirt.gltf');
    // this.engServ.loadModel('assets/characters/man/clothes/trouser/man_trouser_blue.gltf');
    // this.engServ.loadModel('assets/characters/man/accessories/shoes/mShoesBlack.gltf');



  }
  change() {
    this.engServ.loadSkin(this.skinMale.skinBlack.images, this.skinMale.skinBlack.children, 1);
  }



  fun() {
    this.engServ.loadSkin(this.skinMale.skinWhite.images, this.skinMale.skinWhite.children, 1);
    // this.engServ.loadOthers('assets/characters/man/body/hair/mHairBlack.png', 'man_hair_blac', 0);
    // this.engServ.loadOthers(this.trousermale.style1.image, this.trousermale.style1.children, 2)
    // this.engServ.loadOthers(this.tshirtMale.style1.image, this.tshirtMale.style1.children, 1)
    // this.engServ.loadOthers('assets/characters/man/accessories/shoes/running_shoes_c_4952.jpg', 'shoes', 1)

  }

  onSubmit(f: NgForm) {
    let names = this.skinMale.skinWhite.children;
    let value = f.value;

    let Height = value['Height'];
    let Chest = value['Chest'];
    let Neck = value['Neck'];
    let Sleeve = value['Sleeve'];
    let Hip = value['Hip'];
    let Shoulder = value['Shoulder'];
    let Waist = value['Waist'];

    if (Array.isArray(names)) {
      names.forEach((element, index) => {
        this.engServ.scene.traverse((o) => {
          if (o instanceof THREE.Mesh) {
            if (o.name === names[index]) {
              o.morphTargetInfluences[o.morphTargetDictionary["chestmax"]] = (Chest - 70) / 80;
              o.morphTargetInfluences[o.morphTargetDictionary["heightMax"]] = (Height - 150) / 70;
              o.morphTargetInfluences[o.morphTargetDictionary["neckMax"]] = (Neck - 32) / 22;
              o.morphTargetInfluences[o.morphTargetDictionary["neckToWristMax"]] = (Sleeve - 69) / 31;
              o.morphTargetInfluences[o.morphTargetDictionary["hipMAX"]] = (Hip - 65) / 89;
              o.morphTargetInfluences[o.morphTargetDictionary["shoulderMax"]] = (Shoulder - 34) / 16;
              o.morphTargetInfluences[o.morphTargetDictionary["waistMax"]] = (Waist - 54) / 104;
            }
          }
        });
      });
    }
  }

  ngAfterViewInit(): void {
    this.sub = this.engServ.loadMTL.subscribe(res => {
      if (res == true) {
        this.fun();
      }
    });
  }
  ngOnDestroy(): void {
    if (this.engServ.frameId != null) {
      cancelAnimationFrame(this.engServ.frameId);
    }
    this.sub.unsubscribe();
  }
}
