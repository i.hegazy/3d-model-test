import { Component, OnInit, ViewChild, ElementRef, AfterViewInit, OnDestroy } from '@angular/core';
import { EngineService } from 'src/app/engine.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-female',
  templateUrl: './female.component.html',
  styleUrls: ['./female.component.scss']
})
export class FemaleComponent implements OnInit, AfterViewInit, OnDestroy {

  @ViewChild('rendererCanvas', { static: true })
  public rendererCanvas: ElementRef<HTMLCanvasElement>;
  sub: Subscription;

  skinWomen = {
    skinWhite: {
      images: [
        "assets/characters/women/body/basic/skinColorWhite/women_face_white.jpg",
        "assets/characters/women/body/basic/skinColorWhite/women_west_white.jpg",
        "assets/characters/women/body/basic/skinColorWhite/women_leg_white.jpg",
      ],
      children: [
        "body.005_1",
        "body.005_0",
        "body.005_2"
      ]

    },
    skinBlack: {
      images: [
        "assets/characters/women/body/basic/skinColorBlack/women_face_black.jpg",
        "assets/characters/women/body/basic/skinColorBlack/women_west_black.jpg",
        "assets/characters/women/body/basic/skinColorBlack/women_leg_black.jpg",
      ],
      children: [
        "body.005_1",
        "body.005_0",
        "body.005_2"
      ]
    }
  };


  tshirtsWomen = {
    style1: {
      images: [
        "assets/characters/women/clothes/wshirt/Check_Madras_white2.jpg"
      ],
      children: [
        "wshirt"
      ]
    }


  }

  skirtsWomen = {
    style1: {
      images: [
        "assets/characters/women/clothes/wSkirt/flowerBlack.jpg"
      ],
      children: [
        "wSkirt"
      ]
    }
  }
  constructor(private engServ: EngineService) { }

  ngOnInit() {

    this.engServ.getCanvas(this.rendererCanvas);
    this.engServ.createRenderer();
    this.engServ.createScene();
    this.engServ.createCamera();
    this.engServ.createLight();
    this.engServ.animate();
    this.engServ.addControls();

    this.engServ.loadModel('assets/characters/women/body/basic/wBody.gltf');
    this.engServ.loadModel('assets/characters/women/body/hair/wHairBlack.gltf');
    this.engServ.loadOthers('assets/characters/women/body/hair/man_hair_black.png', 'wHair', 2);


    this.engServ.loadModel('assets/characters/women/body/eays/wEays.gltf');
    this.engServ.loadModel('assets/characters/women/accessories/shoes/wShoesBlack.glb');
    this.engServ.loadModel('assets/characters/women/clothes/wshirt/wshirt.gltf');
    this.engServ.loadModel('assets/characters/women/clothes/wSkirt/wSkirt.gltf');
  }

  sethair() {
    this.engServ.loadOthers('assets/characters/women/body/hair/man_hair_black.png', 'wHair', 2);
    this.engServ.render()
  }

  fun() {
    this.engServ.loadSkin(this.skinWomen.skinWhite.images, this.skinWomen.skinWhite.children, 1);
    this.engServ.loadOthers('assets/characters/women/body/hair/man_hair_black.png', 'wHair', 2);
    this.engServ.loadOthers(this.tshirtsWomen.style1.images, this.tshirtsWomen.style1.children, 1)
    this.engServ.loadOthers(this.skirtsWomen.style1.images, this.skirtsWomen.style1.children, 1)

  }
  ngAfterViewInit(): void {
    this.sub = this.engServ.loadMTL.subscribe(res => {
      if (res == true) {
        this.fun();
      }
    });
  }
  ngOnDestroy(): void {
    if (this.engServ.frameId != null) {
      cancelAnimationFrame(this.engServ.frameId);
    }
    this.sub.unsubscribe();
  }
}
