import { Component, OnInit, ElementRef, ViewChild, AfterViewInit, OnDestroy } from '@angular/core';
import { EngineService } from 'src/app/engine.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-male-kid',
  templateUrl: './male-kid.component.html',
  styleUrls: ['./male-kid.component.scss']
})
export class MaleKidComponent implements OnInit, AfterViewInit, OnDestroy {

  @ViewChild('rendererCanvas', { static: true })
  public rendererCanvas: ElementRef<HTMLCanvasElement>;
  sub: Subscription;
  skinBoy = {
    skinWhite: {
      images: [
        "assets/characters/boy/body/basic/skinColorWhite/boy_face_white.jpg",
        "assets/characters/boy/body/basic/skinColorWhite/boy_west_white.jpg",
        "assets/characters/boy/body/basic/skinColorWhite/boy_leg_white.jpg",

      ],
      children: [
        "bBody_2",
        "bBody_1",
        "bBody_0"
      ]

    },
    skinBlack: {
      images: [
        "assets/characters/boy/body/basic/skinColorBlack/boy_face_black.jpg",
        "assets/characters/boy/body/basic/skinColorBlack/boy_west_black.jpg",
        "assets/characters/boy/body/basic/skinColorBlack/boy_leg_black.jpg",
      ],
      children: [
        "bBody_2",
        "bBody_1",
        "bBody_0"

      ]
    }
  };

  constructor(private engServ: EngineService) { }

  ngOnInit() {
    this.engServ.getCanvas(this.rendererCanvas);
    this.engServ.createRenderer();
    this.engServ.createScene();
    this.engServ.createCamera();
    this.engServ.createLight();
    this.engServ.animate();
    this.engServ.addControls();

    this.engServ.loadModel('assets/characters/boy/body/basic/bBody.gltf');
    this.engServ.loadModel('assets/characters/boy/body/eays/bEays.gltf');
    this.engServ.loadModel('assets/characters/boy/body/hair/bHair.glb');
    this.engServ.loadModel('assets/characters/boy/accessories/shoes/bShoes.glb');
    this.engServ.loadModel('assets/characters/boy/clothes/tshirt/bTshirt.glb');
    this.engServ.loadModel('assets/characters/boy/clothes/short/bShort.glb');

  }
  fun() {
    this.engServ.loadSkin(this.skinBoy.skinWhite.images, this.skinBoy.skinWhite.children, 1);
    this.engServ.loadOthers('assets/characters/boy/body/eays/eye_3.jpg', "bEays", 1);
    this.engServ.loadOthers('assets/characters/boy/body/hair/bHair.jpg', "bHair", 1);
    this.engServ.loadOthers('assets/characters/boy/accessories/shoes/bShoes.png', "bShoes", 1);
    this.engServ.loadOthers('assets/characters/boy/clothes/tshirt/bTshirtS1.jpg', "bTshirt", 1);
    this.engServ.loadOthers('assets/characters/boy/clothes/short/bShortS1.png', "bShort", 1);

  }
  ngAfterViewInit(): void {
    this.sub = this.engServ.loadMTL.subscribe(res => {
      if (res == true) {
        this.fun();
      }
    });
  }
  ngOnDestroy(): void {
    if (this.engServ.frameId != null) {
      cancelAnimationFrame(this.engServ.frameId);
    }
    this.sub.unsubscribe();
  }

}
