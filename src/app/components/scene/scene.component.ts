import { EngineService } from './../../engine.service';
import { Component, OnDestroy, ViewChild, ElementRef, OnInit, AfterViewInit } from '@angular/core';
import { Subscription } from 'rxjs';
//import file from '../../../assets/body/skineBlack'

@Component({
  selector: 'app-scene',
  templateUrl: './scene.component.html',
  styleUrls: ['./scene.component.scss']
})
export class SceneComponent implements OnInit, OnDestroy, AfterViewInit {
  @ViewChild('rendererCanvas', { static: true })
  public rendererCanvas: ElementRef<HTMLCanvasElement>;
  sub: Subscription;
  skinMale = {
    skinWhite: {
      images: [
        "assets/characters/man/body/basic/skinColorWhite/man_face_white.jpg",
        "assets/characters/man/body/basic/skinColorWhite/man_west_white.jpg",
        "assets/characters/man/body/basic/skinColorWhite/man_leg_white.jpg",
      ],
      children: [
        "body.001_2",
        "body.001_1",
        "body.001_0"
      ]

    },
    skinBlack: {
      images: [
        "assets/characters/man/body/basic/skinColorBlack/man_face_black.jpg",
        "assets/characters/man/body/basic/skinColorBlack/man_west_black.jpg",
        "assets/characters/man/body/basic/skinColorBlack/man_leg_black.jpg",
      ],
      children: [
        "body.001_2",
        "body.001_1",
        "body.001_0"

      ]
    }
  };

  trousermale = {
    style1: {
      image:
        ['assets/characters/man/clothes/trouser/Polyester_Viscose_Mens_Trouser.jpg'],
      children: ["trouser_0", "trouser_1", "trouser_2", "trouser_3"]

    }
  }

  tshirtMale = {
    style1: {
      image:
        ['assets/characters/man/clothes/tshirt/Jersey_garay.jpg', "assets/characters/man/clothes/tshirt/Marvelous Desinger_emblem.png"],
      children: ["tshirt_0", "tshirt_1"]

    }

  }
  constructor(private engServ: EngineService) { }

  ngOnInit() {
    this.engServ.getCanvas(this.rendererCanvas);
    this.engServ.createRenderer();
    this.engServ.createScene();
    this.engServ.createCamera();
    this.engServ.createLight();
    this.engServ.animate();
    this.engServ.addControls();

    this.engServ.loadModel('assets/characters/man/body/hair/mHairBlack.gltf');
    this.engServ.loadModel('assets/characters/man/body/basic/mBody.gltf');
    this.engServ.loadModel('assets/characters/man/body/eays/mEays.gltf');
    this.engServ.loadModel('assets/characters/man/clothes/tshirt/tshirt.gltf');
    this.engServ.loadModel('assets/characters/man/clothes/trouser/man_trouser_blue.gltf');
    this.engServ.loadModel('assets/characters/man/accessories/shoes/mShoesBlack.gltf');

  }
  change() {
    this.engServ.loadSkin(this.skinMale.skinBlack.images, this.skinMale.skinBlack.children, 1);

  }

  removeShoes() {
    let shoes = this.engServ.scene.getObjectByName("shoes");
    this.engServ.scene.remove(shoes);
  }
  addShoes() {
    this.engServ.loadModel('assets/characters/man/accessories/shoes/mShoesBlack.gltf');

  }

  fun() {
    this.engServ.loadSkin(this.skinMale.skinWhite.images, this.skinMale.skinWhite.children, 1);
    this.engServ.loadOthers('assets/characters/man/body/hair/mHairBlack.png', 'man_hair_blac', 0);
    this.engServ.loadOthers(this.trousermale.style1.image, this.trousermale.style1.children, 2)
    this.engServ.loadOthers(this.tshirtMale.style1.image, this.tshirtMale.style1.children, 1)
    this.engServ.loadOthers('assets/characters/man/accessories/shoes/running_shoes_c_4952.jpg', 'shoes', 1)

  }
  ngAfterViewInit(): void {
    this.sub = this.engServ.loadMTL.subscribe(res => {
      if (res == true) {
        this.fun();
      }
    });
  }
  ngOnDestroy(): void {
    if (this.engServ.frameId != null) {
      cancelAnimationFrame(this.engServ.frameId);
    }
    this.sub.unsubscribe();
  }
}
