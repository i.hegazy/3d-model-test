import { Component, OnInit, ElementRef, ViewChild, OnDestroy } from '@angular/core';
import { EngineService } from 'src/app/engine.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-female-kid',
  templateUrl: './female-kid.component.html',
  styleUrls: ['./female-kid.component.scss']
})
export class FemaleKidComponent implements OnInit, OnDestroy {

  @ViewChild('rendererCanvas', { static: true })
  public rendererCanvas: ElementRef<HTMLCanvasElement>;
  sub: Subscription;
  skinGirl = {
    skinWhite: {
      images: [
        "assets/characters/girl/body/basic/skinColorWhite/girl_face_white.jpg",
        "assets/characters/girl/body/basic/skinColorWhite/girl_west_white.jpg",
        "assets/characters/girl/body/basic/skinColorWhite/girl_leg_white.jpg",

      ],
      children: [
        "gBody_2",
        "gBody_1",
        "gBody_0"
      ]

    },
    skinBlack: {
      images: [
        "assets/characters/girl/body/basic/skinColorBlack/girl_face_black.jpg",
        "assets/characters/girl/body/basic/skinColorBlack/girl_west_black.jpg",
        "assets/characters/girl/body/basic/skinColorBlack/girl_leg_black.jpg",
      ],
      children: [
        "gBody_2",
        "gBody_1",
        "gBody_0"

      ]
    }
  };

  constructor(private engServ: EngineService) { }

  ngOnInit() {
    this.engServ.getCanvas(this.rendererCanvas);
    this.engServ.createRenderer();
    this.engServ.createScene();
    this.engServ.createCamera();
    this.engServ.createLight();
    this.engServ.animate();
    this.engServ.addControls();

    this.engServ.loadModel('assets/characters/girl/body/basic/gBody.glb');
    this.engServ.loadModel('assets/characters/girl/body/eays/gEays.glb');
    this.engServ.loadModel('assets/characters/girl/body/hair/gHair.glb');
    this.engServ.loadModel('assets/characters/girl/accessories/shoes/gShoes.glb');
    this.engServ.loadModel('assets/characters/girl/clothes/dress/gDress.glb');

  }
  fun() {
    this.engServ.loadSkin(this.skinGirl.skinWhite.images, this.skinGirl.skinWhite.children, 1);
    this.engServ.loadOthers('assets/characters/girl/body/eays/eye_3.jpg', "gEays", 1);
    this.engServ.loadOthers('assets/characters/girl/body/hair/gHair.jpg', "gHair", 1);
    this.engServ.loadOthers('assets/characters/girl/accessories/shoes/gShoes.jpg', "gShoes", 1);
    this.engServ.loadOthers('assets/characters/girl/clothes/dress/gDress.jpg', "gDress", 1);

  }

  ngAfterViewInit(): void {
    this.sub = this.engServ.loadMTL.subscribe(res => {
      if (res == true) {
        this.fun();
      }
    });
  }
  ngOnDestroy(): void {
    if (this.engServ.frameId != null) {
      cancelAnimationFrame(this.engServ.frameId);
    }
    this.sub.unsubscribe();
  }


}
